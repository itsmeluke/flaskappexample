from flask import Flask

def create_app(**kwargs):
    app = Flask(__name__)

    @app.route('/')
    def index():
        return 'Hello! Route index! Deployed by bitbucket 94'

    app.url_map.strict_slashes = False
    
    return app