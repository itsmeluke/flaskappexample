FROM python:3.7

ADD . /flasktest
WORKDIR /flasktest

RUN pip install pipenv
# RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt

EXPOSE 5000

# CMD "python manage.py db migrate && python manage.py db upgrade &&  python app.py"
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]