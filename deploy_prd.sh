echo 'Login to AWS'
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)

echo 'Docker build'
docker build -t flaskapp .
docker tag flaskapp:latest $ECR_IMAGE_URI:$ECR_IMAGE_TAG

echo 'Push image to ECR'
docker push $ECR_IMAGE_URI:$ECR_IMAGE_TAG

echo 'Kill task and start a new one'
aws ecs list-tasks --cluster $CLUSTER_NAME | jq -r ".taskArns[]" | awk '{print "aws ecs stop-task --cluster $CLUSTER_NAME --task \""$0"\""}' | sh
aws ecs update-service --cluster $CLUSTER_NAME --force-new-deployment  --service web-app-service