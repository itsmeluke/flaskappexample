import threading
import requests
from random import randint
from time import sleep
from tqdm import tqdm
from core import create_app

if __name__ == '__main__':
    app = create_app()
    
    def make_request():
        while True:
            print('Making request: {}'.format(threading.active_count()))
            response = requests.get('https://5e20c92de31c6e0014c60b4a.mockapi.io/api/users')
            sleep(randint(1, 5))

    def run_app():
        print('starting web server')
        app.run(host='0.0.0.0')

    def watcher():
        while True:
            if threading.active_count() < 500:
                qnt = 500 - threading.active_count()
                for i in range(qnt):
                    threading.Thread(target=make_request).start()

    
    threading.Thread(target=run_app).start()
    threading.Thread(target=watcher).start()